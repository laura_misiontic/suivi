# Tracing



## Description
Tracing es una aplicación móvil que tiene como propósito permitirle al usuario hacer seguimiento de sus actividades de trabajo. Esta aplicación permite hacer el registro de varios usuarios. Cuenta con tres opciones principales: la primera para crear actividades, la segunda para consultar las actividades registradas, hacerles seguimiento y visualizar su estado (ok, pendiente, en trámite, en proceso) y la tercera para consultar páginas de interés para el usuario. Además, esta aplicación cuenta con la opción de asignar alarmas que le generen al usuario un recordatorio de las actividades que tiene pendientes, dándole la opción de posponerlas de ser necesario.
